<?php
    $numberOfSections = 4;
    
    $titles = array("¿En serio?", "¿Dónde va Vicente?", "¿Como ye, ho?", "Contadme un cuento", "Lo que yo te diga", "Tira que libras");
    $required = $_POST['required'];
    $deleted = $_POST['deleted'];

    if(empty($required)) {
        $required = array("¿En serio?");
    }
    
    $titles = array_diff($titles, $required);
    $optional = array_merge(array_diff($titles, $required));

    if (!empty($deleted)) {
        $optional = array_merge(array_diff($optional, $deleted));
    } else {
        $deleted = array("");
    }
    
    $numberOfSections -= count($required);
    $today = $required;
    
    while ($numberOfSections > 0) {
        $randomValue = rand (0, count($optional) - 1);
        array_push($today, $optional[$randomValue]);
        array_splice($optional, $randomValue, 1);
        $numberOfSections --;
    }
    
    shuffle($today);
?>
<!doctype html>
<html lang="es">
    <head>
        <title>Fate button: Anécdotas de triatlón, y otros deportes de resistencia</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    </head>
    
    <body>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

        <header>
          <nav class="container navbar navbar-expand-lg navbar-dark bg-dark">
            <h1 class="text-white"> Fate button </h1>
          </nav>
        </header>
        
        <div class="container">
          <div class="row">
            <div class="col">
                <div class="card w-100">
                    <img class="card-img-top" src="img/caratula.jpg" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"> ... don't mistake coincidence for fate ... </h5>
                        <p class="card-text"><a href="https://anchor.fm/anecdotas-triatlon"> Anécdotas de triatlón y otros deportes de resistencia </a></p>
                    </div>
                    <ul id="todaySections" class="list-group list-group-flush">
                        <?php $i = 1;
                                foreach ($today as $section) {
                                $id = "section" . $i++;
                        ?>
                        <li class="list-group-item" id="<?php echo $id ?>" style="display:none"> <?php echo $section ?> </li>
                        <?php } ?>
                    </ul>
                    <div class="card-body">
                        <button class="btn btn-primary" id="fate" type="button">I'm Feeling Lucky</button>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card w-100">
                    <div class="card-header">
                        <h5 class="card-title"> Secciones </h5>
                    </div>
                    <form method="post">
                        <ul id="sections" class="list-group list-group-flush">
                            <li class="list-group-item">
                            <img src="img/delete.png" style="width: .9rem; height: .9rem"/>
                            <img src="img/required.png" style="width: .9rem; height: .9rem"/>
                            </li>
                            <?php
                                foreach ($required as $section) {
                            ?>
                            <li class="list-group-item">
                                <input <?php echo (in_array($section, $deleted) ? 'checked' : '')?> type="checkbox" name="deleted[]" value="<?php echo $section ?>"></input>
                                <input checked type="checkbox" name="required[]" value="<?php echo $section ?>"> <?php echo $section ?></input>
                            </li>
                            <?php } ?>

                            <?php
                                foreach ($titles as $section) {
                            ?>
                            <li class="list-group-item">
                                <input <?php echo (in_array($section, $deleted) ? 'checked' : '')?> type="checkbox" name="deleted[]" value="<?php echo $section ?>"></input>

                                <input type="checkbox" name="required[]" value="<?php echo $section ?>"> <?php echo $section ?></input>
                            </li>
                            <?php } ?>
                        </ul>

                        <div class="card-body">
                            <input type="submit" value="Limpiar y configurar" class="btn btn-primary"></input>
                        </div>
                    </form>
                </div>
            </div>
          </div>
        </div>

        <script>
            var currentSection = 1
            var numberOfSections = document.querySelectorAll("#todaySections li").length

            fate.onclick = function(e) {
                var element = document.getElementById ("section" + currentSection)
                element.removeAttribute("style")
                
                if (currentSection === numberOfSections) {
                    fate.setAttribute("disabled", "disabled")
                }
                
                currentSection++
            }
        </script>
    </body>
</html>
