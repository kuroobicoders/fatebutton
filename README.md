# Fate Button

## How to build docker image

```bash
docker build --no-cache -t fatebutton:1.0 .
```

## How to run this image

```bash
docker run --name fatebutton -p 80:80 fatebutton:1.0
```

Now, you can open your browser and visit [http://localhost](http://localhost)
